import React from 'react'
import { Menu } from 'antd'
function MenuPopover() {
	return (
		<Menu>
			<Menu.Item key='1'>New Project</Menu.Item>
			<Menu.Item key='2'>New Group</Menu.Item>
			<Menu.Item key='3'>New Snippet</Menu.Item>
		</Menu>
	)
}

export default MenuPopover
