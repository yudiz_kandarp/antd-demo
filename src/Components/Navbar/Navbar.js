import React from 'react'
import { Layout, Button, Space, Cascader, Popover, Input } from 'antd'
import { MenuOutlined, PlusOutlined, SearchOutlined } from '@ant-design/icons'
import MenuPopover from '../MenuPopover/MenuPopover'
import './navbar.scss'

const { Header } = Layout

function Navbar() {
	const options = [
		{
			value: 'Projects',
			label: 'Projects',
			children: [
				{
					value: 'ant design',
					label: 'ant design',
				},
			],
		},
		{
			value: 'Groups',
			label: 'Groups',
			children: [
				{
					value: 'Mern Stack',
					label: 'Mern Stack',
				},
			],
		},
	]

	return (
		<Header
			style={{
				padding: 0,
				height: '40px',
				display: 'flex',
				alignItems: 'center',
			}}
		>
			<Space align='center'>
				<Cascader
					options={options}
					placeholder='Menu'
					placement='bottomLeft'
					style={{ opacity: 0 }}
				>
					<Button icon={<MenuOutlined />} type='text' className='buttons'>
						Menu
					</Button>
				</Cascader>
			</Space>
			<Space>
				<Popover placement='bottom' content={<MenuPopover />} trigger='click'>
					<Button
						icon={<PlusOutlined />}
						type='text'
						className='buttons'
					></Button>
				</Popover>
				<Input placeholder='Search' prefix={<SearchOutlined />} />
			</Space>
		</Header>
	)
}

export default Navbar
