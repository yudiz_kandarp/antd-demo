import React, { useState } from 'react'
import {
	DoubleLeftOutlined,
	DoubleRightOutlined,
	LoginOutlined,
	FormOutlined,
	TableOutlined,
	HomeOutlined,
	CalendarOutlined,
	FieldTimeOutlined,
} from '@ant-design/icons'
import { Layout, Menu } from 'antd'
import { NavLink, useLocation } from 'react-router-dom'
const { Sider } = Layout

function Sidebar() {
	const location = useLocation()
	const [collapsed, setCollapsed] = useState(false)
	return (
		<Sider
			style={{ background: '#fff' }}
			trigger={
				collapsed ? (
					<DoubleRightOutlined style={{ color: '#000' }} />
				) : (
					<DoubleLeftOutlined style={{ color: '#000' }} />
				)
			}
			collapsible
			collapsed={collapsed}
			onCollapse={() => setCollapsed((prev) => !prev)}
		>
			<div className='logo' />
			<Menu
				defaultSelectedKeys={[location.pathname]}
				mode='inline'
				width='400px'
			>
				<Menu.Item key='/' icon={<HomeOutlined />}>
					<NavLink to='/'>Home</NavLink>
				</Menu.Item>
				<Menu.Item key='/login' icon={<LoginOutlined />}>
					<NavLink to='/login'>Login</NavLink>
				</Menu.Item>
				<Menu.Item key='/cards' icon={<FormOutlined />}>
					<NavLink to='/cards'>Cards</NavLink>
				</Menu.Item>
				<Menu.Item key='/table' icon={<TableOutlined />}>
					<NavLink to='/table'>Table</NavLink>
				</Menu.Item>
				<Menu.Item key='/calendar' icon={<CalendarOutlined />}>
					<NavLink to='/calendar'>Calendar</NavLink>
				</Menu.Item>
				<Menu.Item key='/timeline' icon={<FieldTimeOutlined />}>
					<NavLink to='/timeline'>Timeline</NavLink>
				</Menu.Item>
			</Menu>
		</Sider>
	)
}

export default Sidebar
