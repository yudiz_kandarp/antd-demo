import React from 'react'
import { Layout } from 'antd'
import Sidebar from './Components/SideBar/Sidebar'
import Navbar from './Components/Navbar/Navbar'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './Pages/Home'
import './App.scss'
import Signin from './Pages/Signin'
import Cards from './Pages/Cards'
import Tables from './Pages/Tables'
import CalenderPage from './Pages/CalenderPage'
import TimelinePage from './Pages/TimelinePage'

function App() {
	return (
		<Layout style={{ minHeight: '100vh' }}>
			<Navbar />
			<Layout>
				<BrowserRouter>
					<Sidebar />
					<Routes>
						<Route exact path='/' element={<Home />} />
						<Route path='/login' element={<Signin />} />
						<Route path='/cards' element={<Cards />} />
						<Route path='/table' element={<Tables />} />
						<Route path='/calendar' element={<CalenderPage />} />
						<Route path='/timeline' element={<TimelinePage />} />
					</Routes>
				</BrowserRouter>
			</Layout>
		</Layout>
	)
}

export default App
