import React from 'react'
import { Card, Layout, Breadcrumb } from 'antd'
import { HomeOutlined, UserOutlined } from '@ant-design/icons'

const { Content } = Layout
const { Meta } = Card

function Cards() {
	return (
		<Content
			style={{
				margin: '16px',
				background: '#fff',
			}}
		>
			<Breadcrumb style={{ margin: '16px' }}>
				<Breadcrumb.Item href='/'>
					<HomeOutlined />
				</Breadcrumb.Item>
				<Breadcrumb.Item href='/cards'>
					<UserOutlined />
					Cards
				</Breadcrumb.Item>
			</Breadcrumb>
			<div
				style={{
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					flexDirection: '',
					height: '100%',
				}}
			>
				<Card
					hoverable
					style={{ width: 240, margin: '10px' }}
					cover={
						<img
							alt='nine tails'
							src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
						/>
					}
				>
					<Meta title='nine tails fox' description='www.instagram.com' />
				</Card>
				<Card
					hoverable
					style={{ width: 240, margin: '10px' }}
					cover={
						<img
							alt='nine tails'
							src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
						/>
					}
				>
					<Meta title='nine tails fox' description='www.instagram.com' />
				</Card>
				<Card
					hoverable
					style={{ width: 240, margin: '10px' }}
					cover={
						<img
							alt='nine tails'
							src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png'
						/>
					}
				>
					<Meta title='nine tails fox' description='www.instagram.com' />
				</Card>
			</div>
		</Content>
	)
}

export default Cards
