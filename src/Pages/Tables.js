import React from 'react'
import { Layout, Table } from 'antd'
const { Content } = Layout

function Tables() {
	const dataSource = [
		{
			key: '1',
			name: 'kandarp',
			age: 21,
			address: '10 Downing Street',
		},
		{
			key: '2',
			name: 'dharmjeet',
			age: 20,
			address: '10 Downing Street',
		},
		{
			key: '3',
			name: 'nikunj thesiya',
			age: 22,
			address: '10 Downing Street',
		},
		{
			key: '4',
			name: 'mrunal mehta',
			age: 20,
			address: '10 Downing Street',
		},
	]

	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Age',
			dataIndex: 'age',
			key: 'age',
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
		},
	]
	return (
		<Content style={{ margin: '16px', background: '#fff' }}>
			<Table dataSource={dataSource} columns={columns} />
		</Content>
	)
}

export default Tables
