import React from 'react'
import { Layout, Calendar } from 'antd'
const { Content } = Layout
function CalenderPage() {
	return (
		<Content
			style={{
				margin: '16px',
				background: '#fff',
			}}
		>
			<Calendar />
		</Content>
	)
}

export default CalenderPage
