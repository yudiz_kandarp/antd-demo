import React from 'react'
import {
	Layout,
	Form,
	Input,
	Button,
	Checkbox,
	Breadcrumb,
	Row,
	Col,
} from 'antd'
import { HomeOutlined, UserOutlined } from '@ant-design/icons'
const { Content } = Layout

function Signin() {
	return (
		<Content style={{ margin: '16px', background: '#fff' }}>
			<Breadcrumb style={{ margin: '16px' }}>
				<Breadcrumb.Item href='/'>
					<HomeOutlined />
				</Breadcrumb.Item>
				<Breadcrumb.Item href=''>
					<UserOutlined />
					Signin
				</Breadcrumb.Item>
			</Breadcrumb>
			<Form
				style={{
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					flexDirection: 'column',
					height: '100%',
					width: '100%',
					padding: '32px',
				}}
				name='basic'
				initialValues={{
					remember: true,
				}}
				autoComplete='off'
			>
				<Row>
					<Col span={24}>
						<Form.Item
							label='Username'
							name='username'
							rules={[
								{
									required: true,
									message: 'Please input your username!',
								},
							]}
						>
							<Input />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item
							label='Password'
							name='password'
							rules={[
								{
									required: true,
									message: 'Please input your password!',
								},
							]}
						>
							<Input.Password />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item
							name='remember'
							valuePropName='checked'
							wrapperCol={{
								offset: 0,
								span: 24,
							}}
						>
							<Checkbox>Remember me</Checkbox>
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item
							wrapperCol={{
								offset: 0,
								span: 16,
							}}
						>
							<Button type='primary' htmlType='submit'>
								Submit
							</Button>
						</Form.Item>
					</Col>
				</Row>
			</Form>
		</Content>
	)
}

export default Signin
