import React from 'react'
import { Layout, Timeline } from 'antd'
import { ClockCircleOutlined } from '@ant-design/icons'
const { Content } = Layout
function TimelinePage() {
	return (
		<Content
			style={{
				margin: '16px',
				background: '#fff',
				padding: '16px',
			}}
		>
			<Timeline mode='alternate'>
				<Timeline.Item>Joined yudiz solution at 01-01-2022</Timeline.Item>
				<Timeline.Item color='green'>
					Learning JavaScript from 12-01-2022
				</Timeline.Item>
				<Timeline.Item>
					Learning Fundamentals of React js from 11-02-2022
				</Timeline.Item>
				<Timeline.Item
					color='green'
					dot={<ClockCircleOutlined style={{ fontSize: '16px' }} />}
				>
					Learning React advance concepts
				</Timeline.Item>
			</Timeline>
		</Content>
	)
}

export default TimelinePage
