import React from 'react'
import { Layout, Carousel, Image } from 'antd'

const { Content } = Layout
function Home() {
	return (
		<Content
			style={{
				margin: '16px',
				background: '#fff',
			}}
		>
			<Carousel
				autoplay
				style={{
					minWidth: '100%',
					maxWidth: '100%',
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
				}}
			>
				<div
					style={{
						width: '100%',
						display: 'flex',
						alignItems: 'center',
						justifyContent: 'center',
					}}
				>
					<Image
						style={{ width: '100%' }}
						src='https://images.unsplash.com/photo-1648737966614-55e58b5e3caf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=872&q=80'
					/>
				</div>
				<Image
					style={{}}
					src='https://images.unsplash.com/photo-1648737966614-55e58b5e3caf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=872&q=80'
				/>
			</Carousel>
		</Content>
	)
}

export default Home
