import { createRoot } from 'react-dom/client'
import React from 'react'
import App from './App'
import 'antd/dist/antd.css'
import './index.scss'

const app = document.getElementById('root')
const root = createRoot(app)
root.render(<App />)
